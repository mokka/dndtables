package de.mokka.dndtables.xml;

import de.mokka.dndtables.rollables.tables.GenericTable;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.FileOutputStream;
import java.util.Map;

public class Writer {

	public static void saveTable(GenericTable table, String fileName) throws Exception {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(new FileOutputStream(fileName));
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		XMLEvent nl = eventFactory.createDTD("\n");

		StartDocument startDocument = eventFactory.createStartDocument();
		eventWriter.add(startDocument);
		eventWriter.add(nl);

		StartElement startElement = eventFactory.createStartElement("", "", "table");
		eventWriter.add(startElement);
		Attribute nameAttribute = eventFactory.createAttribute("name", table.getName());
		eventWriter.add(nameAttribute);
		Integer dice = table.getDiceType();
		Attribute diceAttribute = eventFactory.createAttribute("d", dice.toString());
		eventWriter.add(diceAttribute);
		Attribute idAttribute = eventFactory.createAttribute("id", table.getId());
		eventWriter.add(idAttribute);
		eventWriter.add(nl);

		for (Map.Entry<String, String> entry : table.getItems()) {
			createTableItem(eventWriter, entry);
		}

		eventWriter.add(eventFactory.createEndElement("", "", "table"));
		eventWriter.add(nl);
		eventWriter.add(eventFactory.createEndDocument());
		eventWriter.close();
	}

	private static void createTableItem(XMLEventWriter eventWriter, Map.Entry<String, String> entry) throws XMLStreamException {
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		XMLEvent end = eventFactory.createDTD("\n");
		XMLEvent tab = eventFactory.createDTD("    ");

		StartElement startElement = eventFactory.createStartElement("", "", "item");
		eventWriter.add(tab);
		eventWriter.add(startElement);

		Attribute idAttribute = eventFactory.createAttribute("id", entry.getKey().toString());
		eventWriter.add(idAttribute);

		Characters characters = eventFactory.createCharacters(entry.getValue());
		eventWriter.add(characters);

		EndElement endElement = eventFactory.createEndElement("", "", "item");
		eventWriter.add(endElement);
		eventWriter.add(end);
	}
}
