package de.mokka.dndtables.xml;

import de.mokka.dndtables.rollables.Rollable;
import de.mokka.dndtables.rollables.generators.GenericGenerator;
import de.mokka.dndtables.rollables.tables.GenericTable;
import de.mokka.dndtables.util.Tuple;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Parser {
	private static final String TABLE = "table";
	private static final String NAME = "name";
	private static final String DICE = "d";
	private static final String ITEM = "item";
	private static final String ID = "id";
	private static final String GEN = "generator";
	private static final String ENTRY = "entry";
	private static final String TABLEID = "tableID";
	private static final String TABLES = "tables";
	private static final String ROOTID = "rootID";
	private static final String INTERNAL = "internal";
	private static final String GENROOT = "de.mokka.dndtables.rollables.generators";
	private static final String TABLEROOT = "de.mokka.dndtables.rollables.tables";

	@SuppressWarnings({ "unchecked", "null" })
	private static GenericTable parseTable(String fileName) {
		GenericTable table = new GenericTable();
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = Parser.class.getClassLoader().getResourceAsStream(fileName);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			int lastKey = 0;

			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();

					if (startElement.getName().getLocalPart().equals(TABLE)) {
						Iterator<Attribute> attributes = startElement.getAttributes();

						while (attributes.hasNext()) {
							Attribute attribute = attributes.next();
							if (attribute.getName().toString().equals(NAME)) {
								table.setName(attribute.getValue());
							}
							if (attribute.getName().toString().equals(DICE)) {
								String temp = attribute.getValue();
								table.setDiceType(Integer.parseInt(temp));
							}
							if (attribute.getName().toString().equals(ID)) {
								table.setId(attribute.getValue());
							}
						}
					}

					if (startElement.getName().toString().equals(ITEM)) {
						String value;

						value = eventReader.getElementText();

						Iterator<Attribute> attributes = startElement.getAttributes();

						if (attributes.hasNext()) {
							while (attributes.hasNext()) {
								Attribute attribute = attributes.next();
								if (attribute.getName().toString().equals(ID)) {
									StringTokenizer st = new StringTokenizer(attribute.getValue(), "-");
									int keyStart = Integer.parseInt(st.nextToken());
									int keyEnd = keyStart;

									if (st.hasMoreTokens()) {
										keyEnd = Integer.parseInt(st.nextToken());
									}

									for (int i = keyStart; i <= keyEnd; i++) {
										table.put(String.valueOf(i), value);
									}

									lastKey = keyEnd;
								}
							}
						} else {
							lastKey++;
							table.put(String.valueOf(lastKey), value);
						}
					}
				}
			}
		} catch (XMLStreamException e) {
			System.err.println("There was an error parsing the table (" + fileName + "): " + e.getLocalizedMessage());
			e.printStackTrace();
		}

		if (table.getDiceType() == 1) {
			table.setDiceType(table.size());
		}

		return table;
	}

	@SuppressWarnings({ "unchecked", "null" })
	private static GenericGenerator parseGenerator(String fileName) {
		GenericGenerator generator = new GenericGenerator();
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = Parser.class.getClassLoader().getResourceAsStream(fileName);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();

					if (startElement.getName().getLocalPart().equals(GEN)) {
						Iterator<Attribute> attributes = startElement.getAttributes();

						while (attributes.hasNext()) {
							Attribute attribute = attributes.next();
							if (attribute.getName().toString().equals(ID)) {
								generator.setId(attribute.getValue());
							}
							if (attribute.getName().toString().equals(NAME)) {
								generator.setName(attribute.getValue());
							}
						}
					}

					if (startElement.getName().toString().equals(ENTRY)) {
						Iterator<Attribute> attributes = startElement.getAttributes();

						while (attributes.hasNext()) {
							Attribute attribute = attributes.next();
							if (attribute.getName().toString().equals(TABLEID)) {
								generator.put(attribute.getValue(), eventReader.getElementText());
							}
						}
					}
				}
			}
		} catch (XMLStreamException e) {
			System.err.println("There was an error parsing the generator (" + fileName + "): " + e.getLocalizedMessage());
			e.printStackTrace();
		}

		return generator;
	}

	@SuppressWarnings({ "unchecked", "null" })
	public static Map<String, Rollable> loadRollables(String fileName) {
		Map<String, Rollable> tables = new LinkedHashMap<>();
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = Parser.class.getClassLoader().getResourceAsStream(fileName);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();

					if (startElement.getName().toString().equals(TABLE)) {
						Rollable t;
						Tuple<String, Boolean> attributes = extractAttributes(startElement.getAttributes());
						String rootId = attributes.getLeft();
						boolean internal = attributes.getRight();

						String value = eventReader.getElementText();

						if (internal) {
							String classPath = TABLEROOT + "." + value;
							Class<?> cls = Class.forName(classPath);
							t = (Rollable) cls.newInstance();
						} else {
							t = parseTable(value);
						}

						t.setRootID(rootId);

						tables.put(t.getId(), t);
					}

					if (startElement.getName().toString().equals(GEN)) {
						Rollable g;
						Tuple<String, Boolean> attributes = extractAttributes(startElement.getAttributes());
						String rootId = attributes.getLeft();
						boolean internal = attributes.getRight();

						String value = eventReader.getElementText();

						if (internal) {
							String classPath = GENROOT + "." + value;
							Class<?> cls = Class.forName(classPath);
							g = (Rollable) cls.newInstance();
						} else {
							g = parseGenerator(value);
						}

						g.setRootID(rootId);

						tables.put(g.getId(), g);
					}
				}
			}
		} catch (XMLStreamException e) {
			System.err.println("There was an error parsing the table (" + fileName + "): " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("No such class: " + e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException | InstantiationException e) {
			e.printStackTrace();
		}

		return tables;
	}

	private static Tuple<String, Boolean> extractAttributes(Iterator<Attribute> attributes) {

		String rootId = "hidden";
		boolean internal = false;

		while (attributes.hasNext()) {
			Attribute attribute = attributes.next();
			if (attribute.getName().toString().equals(ROOTID)) {
				rootId = attribute.getValue();
			}
			if (attribute.getName().toString().equals(INTERNAL)) {
				internal = Boolean.parseBoolean(attribute.getValue());
			}
		}

		return new Tuple<>(rootId, internal);
	}
}
