package de.mokka.dndtables;

import de.mokka.dndtables.gui.MainFrame;

import javax.swing.*;

public class Main {

	public static void main(String[] args) {

		SwingUtilities.invokeLater(MainFrame::new);
	}
}
