package de.mokka.dndtables.gui;

import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;
import de.mokka.dndtables.rollables.tables.GenericTable;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.HashMap;
import java.util.Map;

class TreeHandler {

	private JTree jTree;
	private Rollable selected;
	private Loader tableLoader;
	private Map<String, DefaultMutableTreeNode> categories;

	TreeHandler() {
		tableLoader = Loader.getInstance();
		selected = new GenericTable();
		categories = new HashMap<>();

		jTree = new JTree(createTree());

		jTree.addTreeSelectionListener(e -> {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) getjTree().getLastSelectedPathComponent();

			if (node == null) return;

			Object nodeInfo = node.getUserObject();

			if (nodeInfo instanceof Rollable) {
				selected = (Rollable) nodeInfo;
			}
		});
	}

	private DefaultMutableTreeNode createTree() {
		DefaultMutableTreeNode root = createCategories();

		for (Rollable rollable : tableLoader.getValues()) {
			addRollable(rollable);
		}

		return root;
	}

	private DefaultMutableTreeNode createCategories() {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Mokka's DnDTables");
		DefaultMutableTreeNode tableRoot = new DefaultMutableTreeNode("Tables");
		DefaultMutableTreeNode genRoot = new DefaultMutableTreeNode("Generators");
		DefaultMutableTreeNode hiddenRoot = new DefaultMutableTreeNode("_hidden");
		DefaultMutableTreeNode froggodNode = new DefaultMutableTreeNode("Frog God Stuff");

		DefaultMutableTreeNode encounters = new DefaultMutableTreeNode("Encounters & Events");
		DefaultMutableTreeNode odd_encounters = new DefaultMutableTreeNode("Odd Encounters");
		DefaultMutableTreeNode items = new DefaultMutableTreeNode("Objects");
		DefaultMutableTreeNode art_objects = new DefaultMutableTreeNode("Art Objects");
		DefaultMutableTreeNode crate_finds = new DefaultMutableTreeNode("Crate Finds");
		DefaultMutableTreeNode gemstones = new DefaultMutableTreeNode("Gemstones");
		DefaultMutableTreeNode npcs = new DefaultMutableTreeNode("NPCs");
		DefaultMutableTreeNode item_properties = new DefaultMutableTreeNode("Item Properties");
		DefaultMutableTreeNode magic_item = new DefaultMutableTreeNode("Magic Items");
		DefaultMutableTreeNode curses = new DefaultMutableTreeNode("Curses");
		DefaultMutableTreeNode locations = new DefaultMutableTreeNode("Locations");

		root.add(tableRoot);
		root.add(genRoot);
		root.add(froggodNode);

		tableRoot.add(encounters);
		encounters.add(odd_encounters);

		tableRoot.add(items);
		items.add(art_objects);
		items.add(crate_finds);
		items.add(gemstones);
		items.add(magic_item);

		tableRoot.add(npcs);

		tableRoot.add(item_properties);

		tableRoot.add(curses);

		tableRoot.add(locations);

		categories.put("tables", tableRoot);
		categories.put("generators", genRoot);
		categories.put("frog_god", froggodNode);
		categories.put("_hidden", hiddenRoot);
		categories.put("encounters", encounters);
		categories.put("odd_encounters", odd_encounters);
		categories.put("items", items);
		categories.put("art_objects", art_objects);
		categories.put("crate_finds", crate_finds);
		categories.put("gemstones", gemstones);
		categories.put("magic_items", magic_item);
		categories.put("item_properties", item_properties);
		categories.put("curses", curses);
		categories.put("npcs", npcs);
		categories.put("locations", locations);

		return root;
	}

	private void addRollable(Rollable rollable) {
		try {
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(rollable);
			categories.get(rollable.getRootID()).add(node);
		} catch (NullPointerException e) {
			System.err.println("The category " + rollable.getRootID() + " does not exist!");
			System.err.println(rollable);
			e.printStackTrace();
		}
	}

	JTree getjTree() {
		return jTree;
	}

	Rollable getSelected() {
		return selected;
	}
}
