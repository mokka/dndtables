package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

public class MutationGenerator extends Rollable {

	private static final String SMALL = "small";
	private static final String MEDIUM = "medium";
	private static final String IMPORTANT = "important";
	private static final String EXTREME = "extreme";

	public MutationGenerator() {
		name = "Mutation Generator";
		id = "mutation_generator";
	}

	@Override
	public String roll() {
		StringBuilder stringBuilder = new StringBuilder();
		Loader loader = Loader.getInstance();
		String effect = loader.get("mutation_2").roll();

		stringBuilder.append("Your body is mutation because of:\n\t").append(loader.get("mutation_1").roll()).append("\n\nThe effect is:\n\t").append(effect).append(":\n\n");

		if (effect.toLowerCase().equals(SMALL)) {
			stringBuilder.append("\t\t").append(loader.get("mutation_3_a").roll()).append("\n\n");
		}

		if (effect.toLowerCase().equals(MEDIUM)) {
			stringBuilder.append("\t\t").append(loader.get("mutation_3_b").roll()).append("\n\n");
		}

		if (effect.toLowerCase().equals(IMPORTANT)) {
			stringBuilder.append("\t\t").append(loader.get("mutation_3_c").roll()).append("\n\n");
		}

		if (effect.toLowerCase().equals(EXTREME)) {
			stringBuilder.append("\t\t").append(loader.get("mutation_3_d").roll()).append("\n\n");
		}

		stringBuilder.append("It can be cured by:\n\t").append(loader.get("mutation_4").roll());

		return stringBuilder.toString();
	}
}
