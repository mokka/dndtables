package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Dice;
import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

public class EastreachNameGenerator extends Rollable {

	private Rollable part_one;
	private Rollable part_two;
	private Rollable part_three;

	private Rollable geo_eastreach;
	private Rollable geo_suilley;

	public EastreachNameGenerator() {
		name = "Eastreach-Exeter Settlement Name";
		id = "eastreach_name_generator";
	}


	@Override
	public String roll() {

		Loader loader = Loader.getInstance();

		part_one = loader.get("se_part_one");
		part_two = loader.get("se_part_two");
		part_three = loader.get("se_part_three");

		geo_eastreach = loader.get("geo_eastreach");
		geo_suilley = loader.get("geo_suilley");

		int type = new Dice(5).roll();

		switch (type) {
			case 1:
				return gen_pattern_a();
			case 2:
				return gen_pattern_b();
			case 3:
				return gen_pattern_c();
			case 4:
				return gen_pattern_d();
			case 5:
				return gen_pattern_e();

			default:
				return "";
		}
	}

	private String gen_pattern_e() {
		return geo_eastreach.roll() + " " + part_one.roll() + part_two.roll().toLowerCase() + part_three.roll()
				.toLowerCase();
	}

	private String gen_pattern_d() {
		return geo_eastreach.roll() + " " + part_one.roll() + part_two.roll().toLowerCase();
	}

	private String gen_pattern_c() {
		return part_one.roll() + part_two.roll().toLowerCase() + part_three.roll().toLowerCase() + " " +
				geo_eastreach.roll();
	}

	private String gen_pattern_b() {
		return part_one.roll() + part_two.roll().toLowerCase() + " " + geo_eastreach.roll();
	}

	private String gen_pattern_a() {

		return part_one.roll() + part_two.roll().toLowerCase() + geo_eastreach.roll().toLowerCase();
	}
}
