package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Dice;
import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

public class PoisonGenerator extends Rollable {

	private static final String STRENGTH = "loss of strength";
	private static final String DEXTERITY = "loss of dexterity";
	private static final String INTELLECT = "loss of intellect";
	private static final String CHARISMA = "loss of charisma";
	private static final String OTHER = "other";
	private static final String DELAYED = "delayed";
	private static final String VERY_WEAK = "very weak";
	private static final String WEAK = "weak";
	private static final String AVERAGE = "average";
	private static final String STRONG = "strong";
	private static final String VERY_STRONG = "very strong";

	public PoisonGenerator() {
		name = "Poison Generator";
		id = "poison_generator";
	}

	@Override
	public String roll() {
		StringBuilder stringBuilder = new StringBuilder();
		Loader loader = Loader.getInstance();
		String effect = loader.get("poison_3").roll();
		StringBuilder speed = new StringBuilder(loader.get("poison_4_a").roll());
		String power = loader.get("poison_4_b").roll();
		String intensity = loader.get("poison_4_c").roll();
		String characteristic = "";
		Rollable symptomTable;

		while (speed.toString().toLowerCase().equals(DELAYED)) {
			String tmp = loader.get("poison_4_a").roll();
			if (! tmp.toLowerCase().equals(DELAYED)) {
				speed.append(" (").append(tmp).append(")");
			}
		}

		Dice damage = new Dice(1);
		Dice symptoms = new Dice(1);
		int damageMultiplier = 1;
		int symptomMultiplier = 1;
		int symptomModifier = 0;
		int symptomsAmount;

		switch (effect.toLowerCase()) {
			case STRENGTH:
				characteristic = "Strength";
				symptomTable = loader.get("poison_5_a");
				break;
			case DEXTERITY:
				characteristic = "Dextrerity";
				symptomTable = loader.get("poison_5_b");
				break;
			case INTELLECT:
				characteristic = "Intellect";
				symptomTable = loader.get("poison_5_c");
				break;
			case CHARISMA:
				characteristic = "Charisma";
				symptomTable = loader.get("poison_5_d");
				break;
			case OTHER:
			default:
				characteristic = "Poison";
				symptomTable = loader.get("poison_5_e");
				break;
		}

		switch (power.toLowerCase()) {
			case VERY_WEAK:
				damage = new Dice(3);
				symptoms = new Dice(2);
				symptomModifier = - 1;
				break;
			case WEAK:
				damage = new Dice(6);
				symptoms = new Dice(2);
				break;
			case AVERAGE:
			default:
				damage = new Dice(12);
				symptoms = new Dice(3);
				break;
			case STRONG:
				damage = new Dice(6);
				damageMultiplier = 3;
				symptoms = new Dice(2);
				symptomModifier = 1;
				break;
			case VERY_STRONG:
				damage = new Dice(6);
				damageMultiplier = 4;
				symptoms = new Dice(3);
				symptomModifier = 1;
		}

		switch (intensity.toLowerCase()) {
			case WEAK:
			default:
				break;
			case AVERAGE:
				damageMultiplier *= 2;
				symptomModifier *= 2;
				break;
			case STRONG:
				damageMultiplier *= 3;
				symptomModifier *= 3;
		}


		stringBuilder.append("The poison originates from a:\n\t").append(loader.get("poison_1").roll()).append("\n\n").append("It is administered by:\n\t").append(loader.get("poison_2").roll()).append("\n\n").append("The poison's main effect is:\n\t").append(effect).append("\n\n").append("The poison works:\n\t").append(speed).append("\n\n").append("Its power is:\n\t").append(power).append("\n\n").append("Its intensity is:\n\t").append(intensity).append("\n\n").append("The poison deals ").append(damage.roll(damageMultiplier)).append(" points of ").append(characteristic).append(" damage.\n\n").append("The poison causes the following symptom(s):");

		symptomsAmount = symptoms.roll(symptomMultiplier) + symptomModifier;

		for (int i = 0; i < symptomsAmount; i++) {
			stringBuilder.append("\n\t").append(symptomTable.roll());
		}

		stringBuilder.append("\n\n").append("The poison's price is:\n\t").append(loader.get("poison_6").roll());

		return stringBuilder.toString();
	}
}
