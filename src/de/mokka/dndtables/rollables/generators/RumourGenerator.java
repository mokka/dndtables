package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;
import de.mokka.dndtables.util.StringUtil;

import java.util.regex.Pattern;

public class RumourGenerator extends Rollable {

	public RumourGenerator() {
		name = "Rumour Generator";
		id = "rumour_generator";
	}

	@Override
	public String roll() {
		StringBuilder stringBuilder = new StringBuilder();
		Loader loader = Loader.getInstance();

		stringBuilder.append("You know, they say that\n\t").append(loader.get("rumour_a_1").roll()).append("\n\t").
				append(loader.get("rumour_a_2").roll());
		StringUtil.replaceAll(stringBuilder, Pattern.compile("PLACEHOLDER"), loader.get("rumour_a_3").roll());
		stringBuilder.append("\n\nBut then again, I only heard that from\n\t").append(loader.get("rumour_b").roll());

		return stringBuilder.toString();
	}
}
