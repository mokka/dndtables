package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

import java.util.StringTokenizer;

public class SentientWeaponGenerator extends Rollable {

	private static final String LAWFUL = "lawful";
	private static final String CHAOTIC = "chaotic";
	private static final String NEUTRAL = "neutral";
	private static final String GOOD = "good";
	private static final String EVIL = "evil";


	public SentientWeaponGenerator() {
		name = "Sentient Weapon Generator";
		id = "sentient_weapon_generator";
	}

	@Override
	public String roll() {
		Loader loader = Loader.getInstance();
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder ideals = new StringBuilder();
		String alignment = loader.get("sentient_weapon_alignment").roll();

		stringBuilder.append("Abilities:\n\t").append(loader.get("abilities").roll()).append("Communication:\n\t").append(loader.get("sentient_weapon_communication").roll()).append("\n\n").append("Senses:\n\t").append(loader.get("sentient_weapon_senses").roll()).append("\n\n").append("Alignment:\n\t").append(alignment).append("\n\n").append("Mannerisms:\n\t").append(loader.get("npc_mannerisms").roll()).append("\n\n").append("Personality Traits:\n\t").append(loader.get("npc_interaction_traits").roll()).append("\n\n").append("Ideals:\n");

		StringTokenizer strTok = new StringTokenizer(alignment);

		while (strTok.hasMoreTokens()) {
			String token = strTok.nextToken().toLowerCase();

			if (token.equals(LAWFUL)) {
				ideals.append("\t").append(loader.get("npc_ideals_lawful").roll()).append("\n");
			}

			if (token.equals(NEUTRAL)) {
				ideals.append("\t").append(loader.get("npc_ideals_neutral").roll()).append("\n");
			}

			if (token.equals(CHAOTIC)) {
				ideals.append("\t").append(loader.get("npc_ideals_chaotic").roll()).append("\n");
			}

			if (token.equals(GOOD)) {
				ideals.append("\t").append(loader.get("npc_ideals_good").roll()).append("\n");
			}

			if (token.equals(EVIL)) {
				ideals.append("\t").append(loader.get("npc_ideals_evil").roll()).append("\n");
			}
		}

		ideals.append("\t").append(loader.get("npc_ideals_other").roll()).append("\n\n");

		stringBuilder.append(ideals).append("Bonds:\n\t").append(loader.get("npc_bonds").roll()).append("\n\n").append("Flaws:\n\t").append(loader.get("npc_flaws").roll()).append("\n\n").append("Special Purpose:\n\t").append(loader.get("sentient_weapon_special_purpose").roll());

		return stringBuilder.toString();
	}
}
