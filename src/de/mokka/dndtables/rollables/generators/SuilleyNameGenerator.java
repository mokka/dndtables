package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Dice;
import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

public class SuilleyNameGenerator extends Rollable {

	private Rollable part_one;
	private Rollable part_two;
	private Rollable part_three;

	private Rollable geo_eastreach;
	private Rollable geo_suilley;

	public SuilleyNameGenerator() {
		name = "Suilley-Rampart Settlement Name";
		id = "suilley_name_generator";
	}


	@Override
	public String roll() {

		Loader loader = Loader.getInstance();

		part_one = loader.get("se_part_one");
		part_two = loader.get("se_part_two");
		part_three = loader.get("se_part_three");

		geo_eastreach = loader.get("geo_eastreach");
		geo_suilley = loader.get("geo_suilley");

		int type = new Dice(10).roll();

		switch (type) {
			case 1:
			case 2:
			case 3:
			case 4:
				return gen_pattern_a();
			case 5:
				return gen_pattern_b();
			case 6:
				return gen_pattern_c();
			case 7:
				return gen_pattern_d();
			case 8:
				return gen_pattern_e();
			case 9:
				return gen_pattern_f();
			case 10:
				return gen_pattern_g();

				default:
					return "";
		}
	}

	private String gen_pattern_g() {
		return part_one.roll() + part_two.roll().toLowerCase() + part_three.roll().toLowerCase() + " " + geo_eastreach
				.roll();
	}

	private String gen_pattern_f() {
		return part_one.roll() + geo_suilley.roll().toLowerCase();
	}

	private String gen_pattern_e() {
		return part_one.roll() + part_two.roll().toLowerCase() + part_three.roll().toLowerCase() + " " + geo_suilley.roll();
	}

	private String gen_pattern_d() {
		return part_one.roll() + part_two.roll().toLowerCase() + " " + part_one.roll();
	}

	private String gen_pattern_c() {
		return part_one.roll() + " " + part_one.roll() + part_two.roll().toLowerCase() + part_three.roll().toLowerCase();
	}

	private String gen_pattern_b() {
		return part_one.roll() + " " + part_one.roll() + part_two.roll().toLowerCase();
	}

	private String gen_pattern_a() {

		return part_one.roll() + part_two.roll().toLowerCase() + " " + geo_suilley.roll();
	}
}
