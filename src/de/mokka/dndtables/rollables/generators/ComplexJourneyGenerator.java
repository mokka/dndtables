package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Rollable;

public class ComplexJourneyGenerator extends SimpleJourneyGenerator {

	private Rollable objective;
	private Rollable location;
	private Rollable group;
	private Rollable threat;
	private Rollable monsters;
	private Rollable object;
	private Rollable complication;

	public ComplexJourneyGenerator() {
		name = "Compley Journey Generator";
		id = "complex_journey_generator";
	}

	@Override
	protected String roll_journey_details() {
		StringBuilder details = new StringBuilder();

		details.append("The journey's objective is to:\n\t").append(objective.roll()).append("\n\n");
		details.append("The location is (non-city // city):\n\t").append(location.roll()).append("\n\n");
		details.append("The involved group is:\n\t").append(group.roll()).append("\n\n");
		details.append("The immediate threat is:\n\t").append(threat.roll()).append("\n\n");
		details.append("Possibly involved monsters might be:\n\t").append(monsters.roll()).append("\n\n");
		details.append("The related object is:\n\t").append(object.roll()).append("\n\n");
		details.append("A Possible Complication might be:\n\t").append(complication.roll()).append("\n\n");

		return details.toString();
	}

	@Override
	protected void init_tables() {
		super.init_tables();

		objective = loader.get("j_c_objective");
		location = loader.get("j_c_location");
		group = loader.get("j_c_group");
		threat = loader.get("j_c_threat");
		monsters = loader.get("j_c_monsters");
		object = loader.get("j_c_object");
		complication = loader.get("j_c_complication");
	}
}
