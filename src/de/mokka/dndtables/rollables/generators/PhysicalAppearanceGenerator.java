package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

public class PhysicalAppearanceGenerator extends Rollable {

	private static final String MALE = "male";
	private static final String FEMALE = "female";
	private static final String FACIAL_HAIR = "facial hair";

	public PhysicalAppearanceGenerator() {
		name = "Physical Appearance Generator";
		id = "physical_appearance_generator";
	}

	@Override
	public String roll() {
		StringBuilder stringBuilder = new StringBuilder();
		Loader loader = Loader.getInstance();
		String gender = loader.get("physical_appearance_1").roll();
		String feature = loader.get("physical_appearance_10").roll();

		while (feature.toLowerCase().equals(FACIAL_HAIR) && gender.toLowerCase().equals(FEMALE)) {
			feature = loader.get("physical_appearance_10").roll();
		}

		stringBuilder.append("Their gender is:\n\t").append(gender).append("\n\n").append("They look:\n\t").append(loader.get("physical_appearance_2").roll()).append("\n\n").append("Their skin is:\n\t").append(loader.get("physical_appearance_3").roll()).append("\n\n").append("They are:\n\t").append(loader.get("physical_appearance_4_a").roll()).append("\n\n").append("And:\n\t").append(loader.get("physical_appearance_4_b").roll()).append("\n\n").append("Their gait is:\n\t").append(loader.get("physical_appearance_5").roll()).append("\n\n").append("Their hair is:\n\t").append(loader.get("physical_appearance_6_a").roll()).append("\n\t").append(loader.get("physical_appearance_6_b").roll()).append("\n\t").append(loader.get("physical_appearance_6_c").roll()).append("\n\n").append("Their face is:\n\t").append(loader.get("physical_appearance_7").roll()).append("\n\n").append("Their eyes are:\n\t").append(loader.get("physical_appearance_8_a").roll()).append("\n\t").append(loader.get("physical_appearance_8_b").roll()).append("\n\n").append("They look:\n\t").append(loader.get("physical_appearance_9").roll()).append("\n\n").append("Their particular feature is:\n\t").append(feature).append("\n\n");

		if (feature.toLowerCase().equals(FACIAL_HAIR) && gender.toLowerCase().equals(MALE)) {
			stringBuilder.append("His facial hair is:\n\t").append(loader.get("physical_appearance_10_5").roll());
		}

		return stringBuilder.toString();
	}
}
