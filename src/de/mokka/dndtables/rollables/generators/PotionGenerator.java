package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

public class PotionGenerator extends Rollable {

	public PotionGenerator() {
		name = "Potion Generator";
		id = "potion_generator";
	}

	@Override
	public String roll() {
		StringBuilder stringBuilder = new StringBuilder();
		Loader loader = Loader.getInstance();

		stringBuilder
				.append("This is a:\n\t").append(loader.get("potion_title").roll())
				.append(".\n\nThis potion's strength is:\n\t").append(loader.get("potion_strength").roll())
				.append("\n\nThe potion's main effect is:\n\t").append(loader.get("potion_effects").roll())
				.append("\n\nThe potion also temporarily causes:\n\t").append(loader.get("potion_side_effects").roll())
				.append("\n\nThe potion is in:\n\t").append(loader.get("potion_containers").roll())
				.append("\n\nThe potion is:\n\t").append(loader.get("potion_texture").roll())
				.append("\n\nThe potion looks:\n\t").append(loader.get("potion_appearance").roll())
				.append("\n\nWith:\n\t").append(loader.get("potion_appearance_2").roll())
				.append("\n\nThe potion smells like:\n\t").append(loader.get("potion_taste").roll())
				.append("\n\nBut it tastes like:\n\t").append(loader.get("potion_taste").roll())
				.append("\n\nThe potion has a label showing:\n\t").append(loader.get("potion_label").roll());

		return stringBuilder.toString();
	}
}
