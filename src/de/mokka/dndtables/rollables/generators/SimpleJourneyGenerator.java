package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;
import de.mokka.dndtables.rollables.tables.GenericTable;
import javafx.util.Pair;

public class SimpleJourneyGenerator extends Rollable {

	private GenericTable destination;
	private Rollable dest_aachen;
	private Rollable dest_amrin;
	private Rollable dest_eastreach;
	private Rollable dest_exeter;
	private Rollable dest_gaelon;
	private Rollable dest_keston;
	private Rollable dest_rampart;
	private Rollable dest_suilley;
	private Rollable dest_toullen;
	private Rollable dest_unclaimed;
	private Rollable dest_vourdon;
	private Rollable dest_yolbiac;

	private Rollable patron;
	private Rollable patron_mot;

	private Rollable journey_details;
	private Rollable wrap_up;

	protected Loader loader = null;

	private boolean init = false;

	public SimpleJourneyGenerator() {
		name = "Simple Journey Generator";
		id= "simple_journey_generator";
	}

	@Override
	public String roll() {
		if (!init)
				init_tables();

		StringBuilder journey = new StringBuilder();

		journey.append("The journey's destination is in:\n\t");

		Pair<Integer, String> dest = destination.rollFull();

		if (dest.getKey() >= 1 && dest.getKey() <= 9)
				journey.append(dest_aachen.roll());
		else if (dest.getKey() >= 10 && dest.getKey() <= 18)
			journey.append(dest_amrin.roll());
		else if (dest.getKey() >= 19 && dest.getKey() <= 27)
			journey.append(dest_eastreach.roll());
		else if (dest.getKey() >= 28 && dest.getKey() <= 36)
			journey.append(dest_exeter.roll());
		else if (dest.getKey() >= 37 && dest.getKey() <= 45)
			journey.append(dest_gaelon.roll());
		else if (dest.getKey() >= 46 && dest.getKey() <= 54)
			journey.append(dest_keston.roll());
		else if (dest.getKey() >= 55 && dest.getKey() <= 63)
			journey.append(dest_rampart.roll());
		else if (dest.getKey() >= 64 && dest.getKey() <= 72)
			journey.append(dest_suilley.roll());
		else if (dest.getKey() >= 73 && dest.getKey() <= 81)
			journey.append(dest_toullen.roll());
		else if (dest.getKey() >= 82 && dest.getKey() <= 86)
			journey.append(dest_unclaimed.roll());
		else if (dest.getKey() >= 87 && dest.getKey() <= 95)
			journey.append(dest_vourdon.roll());
		else if (dest.getKey() >= 96 && dest.getKey() <= 100)
			journey.append(dest_yolbiac.roll());

		journey.append(", ").append(dest.getValue()).append("\n\n");

		journey.append("The journey's patron is:\n\t").append(patron.roll()).append("\n\nAnd their motivation is:\n\t")
				.append(patron_mot.roll()).append("\n\n");

		journey.append(roll_journey_details());

		journey.append("Wrap-Up:\n\t").append(wrap_up.roll());

		return journey.toString();
	}

	protected String roll_journey_details() {
		return "The journey's objective is to:\n\t" + journey_details.roll() + "\n\n";
	}

	protected void init_tables() {

		loader = Loader.getInstance();

		destination = (GenericTable) loader.get("j_destination");
		dest_aachen = loader.get("j_dest_aachen");
		dest_amrin = loader.get("j_dest_amrin");
		dest_eastreach = loader.get("j_dest_eastreach");
		dest_exeter = loader.get("j_dest_exeter");
		dest_gaelon = loader.get("j_dest_gaelon");
		dest_keston = loader.get("j_dest_keston");
		dest_rampart = loader.get("j_dest_rampart");
		dest_suilley = loader.get("j_dest_suilley");
		dest_toullen = loader.get("j_dest_toullen");
		dest_unclaimed = loader.get("j_dest_unclaimed");
		dest_vourdon = loader.get("j_dest_vourdon");
		dest_yolbiac = loader.get("j_dest_yolbiac");
		patron = loader.get("j_patron");
		patron_mot = loader.get("j_patron_mot");
		journey_details = loader.get("j_simple");
		wrap_up = loader.get("j_wrapup");

		init = true;
	}
}
