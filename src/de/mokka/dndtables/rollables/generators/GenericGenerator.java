package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

import java.util.LinkedHashMap;
import java.util.Map;

public class GenericGenerator extends Rollable {

	public GenericGenerator() {
		entries = new LinkedHashMap<>();
	}

	public String roll() {
		if (getId().equals("invalid")) return "";

		StringBuilder stringBuilder = new StringBuilder();

		try {

			for (Map.Entry<String, String> entry : entries.entrySet()) {

				String rolled = Loader.getInstance().get(entry.getKey()).roll();
				rolled = rolled.substring(0, 1).toUpperCase() + rolled.substring(1);

				stringBuilder.append(entry.getValue()).append("\n\t").append(rolled).append("\n\n");
			}
		} catch (NullPointerException e) {
			System.err.println("There was an error processing the generator " + name + " (" + id + ")");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}

		return stringBuilder.toString();
	}
}
