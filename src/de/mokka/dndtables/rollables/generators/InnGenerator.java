package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Dice;
import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;
import de.mokka.dndtables.rollables.tables.GenericTable;
import javafx.util.Pair;

import java.util.Random;

public class InnGenerator extends Rollable {

	private Rollable creature;
	private GenericTable creature_adj;
	private Rollable object;
	private Rollable object_adj;
	private GenericTable description;
	private Rollable religious_hostel;
	private Rollable dinner;
	private Rollable drink;

	private Loader loader = null;

	private boolean init = false;

	public InnGenerator() {
		name = "Inn Generator";
		id = "inn_generator";
	}

	@Override
	public String roll() {

		if (!init)
			init_tables();

		StringBuilder inn = new StringBuilder();

		inn.append("The inn is called:\n\tThe ");

		int pattern = Dice.d6.roll();

		switch (pattern) {
			case 1:
				Pair<Integer, String> first_part = creature_adj.rollFull();
				String adjective = first_part.getValue();

				if (first_part.getKey() >= 1 && first_part.getKey() <= 2)
					adjective = Integer.toString(new Random().nextInt(5) + 2);

				inn.append(adjective).append(" ").append(creature.roll());
				break;
			case 2:
				inn.append(creature.roll()).append(" and ").append(creature.roll());
				break;
			case 3:
				inn.append(creature.roll()).append("'s ").append(object.roll());
				break;
			case 4:
				inn.append(creature.roll());
				break;
			case 5:
				inn.append(object_adj.roll()).append(" ").append(object.roll());
				break;
			case 6:
				inn.append(object.roll()).append(" and ").append(object.roll());
				break;

				default:
		}

		Pair<Integer, String> des = description.rollFull();

		inn.append("\n\nThe Inn consists of:\n\t").append(des.getValue()).append("\n\n");

		if (des.getKey() >= 21 && des.getKey() <= 25)
			inn.append("The Religious Hostel is dedicated to:\n\t").append(religious_hostel.roll()).append("\n\n");

		inn.append("What's for dinner?\n\t").append(dinner.roll()).append("\n\n");
		inn.append("The inn is know for its particularly good:\n\t").append(drink.roll());

		return inn.toString();
	}

	private void init_tables() {

		loader = Loader.getInstance();

		creature = loader.get("inn_creature");
		creature_adj = (GenericTable) loader.get("inn_creature_adj");
		object = loader.get("inn_object");
		object_adj = loader.get("inn_object_adj");
		description = (GenericTable) loader.get("inn_description");
		religious_hostel = loader.get("inn_religious_hostel");
		dinner = loader.get("inn_dinner");
		drink = loader.get("inn_drink");
	}
}
