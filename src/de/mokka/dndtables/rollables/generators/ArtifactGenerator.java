package de.mokka.dndtables.rollables.generators;

import de.mokka.dndtables.rollables.Dice;
import de.mokka.dndtables.rollables.Loader;
import de.mokka.dndtables.rollables.Rollable;

public class ArtifactGenerator extends Rollable {

	public ArtifactGenerator() {
		name = "Artifact Generator";
		id = "artifact_generator";
	}

	@Override
	public String roll() {
		Loader loader = Loader.getInstance();
		StringBuilder stringBuilder = new StringBuilder();

		int minor_beneficial = Dice.d4.roll() + 2;
		int major_beneficial = Dice.d4.roll() - 1;
		major_beneficial = ( major_beneficial < 1 ) ? 1 : major_beneficial;
		int minor_detrimental = Dice.d4.roll();
		int major_detrimental = Dice.d4.roll() - 2;
		major_detrimental = ( major_detrimental < 1 ) ? 1 : minor_detrimental;

		stringBuilder.append("The artifact has the following minor beneficial properties:\n");

		for (int i = 0; i < minor_beneficial; i++) {
			stringBuilder.append("\t").append(loader.get("artifact_minor_beneficial_item_properties").roll()).append("\n");
		}

		stringBuilder.append("\n\n").append("The artifact has the following major beneficial properties:\n");

		for (int i = 0; i < major_beneficial; i++) {
			stringBuilder.append("\t").append(loader.get("artifact_major_beneficial_item_properties").roll()).append("\n");
		}

		stringBuilder.append("\n\n").append("The artifact has the following minor detrimental properties:\n");

		for (int i = 0; i < minor_detrimental; i++) {
			stringBuilder.append("\t").append(loader.get("artifact_minor_detrimental_item_properties").roll()).append("\n");
		}

		stringBuilder.append("\n\n").append("The artifact has the following major detrimental properties:\n");

		for (int i = 0; i < major_detrimental; i++) {
			stringBuilder.append("\t").append(loader.get("artifact_major_detrimental_item_properties").roll()).append("\n");
		}

		stringBuilder.append("\n\nBackground:\n").append(loader.get("magic_item_background_generator").roll());

		return stringBuilder.toString();
	}
}
