package de.mokka.dndtables.rollables.tables;


import de.mokka.dndtables.rollables.Dice;
import de.mokka.dndtables.rollables.Rollable;
import javafx.util.Pair;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class GenericTable extends Rollable {
	private int diceType;
	private Dice dice;

	public GenericTable() {
		entries = new LinkedHashMap<>();
		dice = new Dice(1);
		diceType = 1;
	}

	public int getDiceType() {
		return diceType;
	}

	public void setDiceType(int diceType) {
		this.diceType = diceType;
		dice = new Dice(diceType);
	}

	public String toFormattedString() {
		StringBuilder buf = new StringBuilder();

		buf.append(name).append(" (").append(id).append("):\n");

		for (Map.Entry entry : entries.entrySet()) {
			buf.append(entry.getKey()).append("\t|\t").append(entry.getValue()).append("\n");
		}

		return buf.toString();
	}

	public String roll() {
		return rollFull().getValue();
	}

	public Pair<Integer,String> rollFull() {
		if (id.toLowerCase().equals("invalid")) return new Pair<>(-1, "");

		try {
			int rolled = dice.roll();
			return new Pair <>(rolled,entries.get(String.valueOf(rolled)).substring(0, 1).toUpperCase() + entries.get
					(String.valueOf(rolled)).substring(1));
		} catch (NullPointerException e) {
			System.err.println("Error rolling on table " + name + " (" + id + ")");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}

		return new Pair<>(-1, "");
	}

	public Set<Map.Entry<String, String>> getItems() {
		return entries.entrySet();
	}

	public int size() {
		return entries.size();
	}
}
