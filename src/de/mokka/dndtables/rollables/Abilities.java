package de.mokka.dndtables.rollables;

public class Abilities extends Rollable {

	private Dice dice;

	public Abilities() {
		name = "Abilities";
		id = "abilities";
		dice = new Dice(6);
	}

	@Override
	public String roll() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Str: ").append(dice.roll(4, RollTypes.DROP_LOWEST)).append("\tDex: ").append(dice.roll(4, RollTypes.DROP_LOWEST)).append("\tCon: ").append(dice.roll(4, RollTypes.DROP_LOWEST)).append("\tWis: ").append(dice.roll(4, RollTypes.DROP_LOWEST)).append("\tInt: ").append(dice.roll(4, RollTypes.DROP_LOWEST)).append("\tCha: ").append(dice.roll(4, RollTypes.DROP_LOWEST)).append("\n\n");

		return stringBuilder.toString();
	}
}
