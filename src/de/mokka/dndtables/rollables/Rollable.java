package de.mokka.dndtables.rollables;

import java.util.Map;

public abstract class Rollable {
	protected String id;
	protected String name;
	protected String rootID;
	protected Map<String, String> entries;

	public Rollable() {
		id = "invalid";
		name = "";
		rootID = "_hidden";
	}

	public void put(String key, String value) {
		entries.put(key, value);
	}

	public String get(String key) {
		return entries.get(key);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRootID() {
		return rootID;
	}

	public void setRootID(String rootID) {
		this.rootID = rootID;
	}

	public abstract String roll();

	@Override
	public String toString() {
		return name;
	}
}
