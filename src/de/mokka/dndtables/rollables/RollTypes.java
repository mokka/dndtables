package de.mokka.dndtables.rollables;

public enum RollTypes {
	NORMAL, DROP_HIGHEST, DROP_LOWEST
}
