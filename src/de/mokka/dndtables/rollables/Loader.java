package de.mokka.dndtables.rollables;

import de.mokka.dndtables.xml.Parser;

import java.util.Collection;
import java.util.Map;

public class Loader {
	private final static String mainFile = "dta/all_tables.xml";

	private static Loader instance = null;
	private Map<String, Rollable> rollables;

	private Loader() {
		rollables = Parser.loadRollables(mainFile);
		rollables.put("abilities", new Abilities());
	}

	public static Loader getInstance() {
		if (instance == null) {
			return new Loader();
		}

		return instance;
	}

	public Rollable get(String key) {
		return rollables.get(key);
	}

	public void put(String key, Rollable value) {
		rollables.put(key, value);
	}

	public Collection<Rollable> getValues() {
		return rollables.values();
	}
}
