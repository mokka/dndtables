package de.mokka.dndtables.rollables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Dice {
	public static final Dice d4 = new Dice(4);
	public static final Dice d6 = new Dice(6);
	public static final Dice d8 = new Dice(8);
	public static final Dice d12 = new Dice(12);
	public static final Dice d20 = new Dice(20);
	public static final Dice d100 = new Dice(100);
	private final int faces;
	private final Random random;

	public Dice(int faces) {
		this.random = new Random();
		this.faces = ( faces < 1 ) ? 1 : faces;
	}

	public int roll() {
		return roll(1);
	}

	public int roll(int n) {
		return roll(n, RollTypes.NORMAL);
	}

	public int roll(int n, RollTypes rollType) {
		List<Integer> rolls = new ArrayList<>();
		int highest = 0;
		int lowest = Integer.MAX_VALUE;

		for (int i = 0; i < n; i++) {
			int roll = 1 + random.nextInt(faces);
			rolls.add(roll);
			highest = ( highest < roll ) ? roll : highest;
			lowest = ( lowest > roll ) ? roll : lowest;
		}

		if (n < 2) {
			rollType = RollTypes.NORMAL;
		}

		if (rollType == RollTypes.DROP_HIGHEST) {
			rolls.remove(Collections.max(rolls));
		}

		if (rollType == RollTypes.DROP_LOWEST) {
			rolls.remove(Collections.min(rolls));
		}

		return rolls.stream().mapToInt(a -> a).sum();
	}
}
