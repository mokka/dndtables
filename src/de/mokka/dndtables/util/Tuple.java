package de.mokka.dndtables.util;

public class Tuple<L, R> {

	private final L left;
	private final R right;

	public Tuple(L left, R right) {
		this.left = left;
		this.right = right;
	}

	public L getLeft() {
		return left;
	}

	public R getRight() {
		return right;
	}

	@Override
	public int hashCode() {
		return left.hashCode() ^ right.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (! ( obj instanceof Tuple )) return false;
		Tuple pairobj = (Tuple) obj;
		return this.left.equals(pairobj.getLeft()) && this.right.equals(pairobj.getRight());
	}
}
