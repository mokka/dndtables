package de.mokka.dndtables.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	public static void replaceAll(StringBuilder stringBuilder, Pattern pattern, String replacement) {
		Matcher m = pattern.matcher(stringBuilder);
		int start = 0;
		while (m.find(start)) {
			stringBuilder.replace(m.start(), m.end(), replacement);
			start = m.start() + replacement.length();
		}
	}
}
